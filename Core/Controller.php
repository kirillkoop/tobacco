<?php

namespace Core;

/**
 * Base controller
 *
 * PHP version 7.0
 */
abstract class Controller
{

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $route_params = [];

    /**
     * Class constructor
     *
     * @param array $route_params  Parameters from the route
     *
     * @return void
     */
    public function __construct($route_params)
    {
        $this->route_params = $route_params;
    }

    /**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name  Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     */
    public function __call($name, $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before()
    {

    }

    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after()
    {
    }

    public static function startSession($isUserActivity=true) {
        if ( session_id() ) return true;
        $sessionLifetime = 7200;

        // Устанавливаем время жизни куки до закрытия браузера (контролировать все будем на стороне сервера)
        ini_set('session.cookie_lifetime', 0);
        ini_set('session.name', 'bts');
        ini_set('session.gc_maxlifetime', $sessionLifetime);
        ini_set('session.gc_probability', 1);
        ini_set('session.gc_divisor', 1);
        if ( ! session_start() ) return false;

        $t = time();

        if ( $sessionLifetime ) {
            // Если таймаут отсутствия активности пользователя задан,
            // проверяем время, прошедшее с момента последней активности пользователя
            // (время последнего запроса, когда была обновлена сессионная переменная lastactivity)
            if ( isset($_SESSION['lastactivity']) && $t-$_SESSION['lastactivity'] >= $sessionLifetime ) {
                // Если время, прошедшее с момента последней активности пользователя,
                // больше таймаута отсутствия активности, значит сессия истекла, и нужно завершить сеанс
                self::clearSession();
            }
            else {
                // Если таймаут еще не наступил,
                // и если запрос пришел как результат активности пользователя,
                // обновляем переменную lastactivity значением текущего времени,
                // продлевая тем самым время сеанса еще на sessionLifetime секунд
                if ( $isUserActivity ) $_SESSION['lastactivity'] = $t;
            }
        }

        return true;
    }

    public static function clearSession()
    {
        if ( session_id() ) {
            // Если есть активная сессия, удаляем куки сессии,
            setcookie(session_name(), session_id(), time()-60*60*24);
            // и уничтожаем сессию
            session_unset();
            session_destroy();
            header('HTTP/1.1 200 OK');
            header('Location: /login/exit');
            exit;
        }

    }
}
