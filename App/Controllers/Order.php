<?php

namespace App\Controllers;

use App\Models\Moysklad;
use \Core\View;
use App\Models\Products;
use App\Models\User;

class Order extends \Core\Controller
{
    protected function before()
    {
        self::startSession();
        if(!isset($_SESSION['user'])){
            header('HTTP/1.1 200 OK');
            header('Location: /login/exit');
            exit;
        }
        $_SESSION['update'] = (isset($_SESSION['update'])) ? $_SESSION['update'] : array();
        $_SESSION['order'] = (isset($_SESSION['order'])) ? $_SESSION['order'] : array();
    }
    public function indexAction()
    {
        $odrer_products = Products::getOrderProducts();
        if($odrer_products){
            foreach ($odrer_products  as $key => $value){
                $product_prices = json_decode($value['prices'],true);
                $odrer_products[$key]['product_price'] = (!$_SESSION['type']) ? $product_prices['BTS'] : $product_prices[$_SESSION['type']];
            }
        }
        View::renderTemplate('Order/index.twig', [
          'admin' => $_SESSION['admin'],
          'type' => $_SESSION['type'],
          'products' => $odrer_products
        ]);
    }

    public function addAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (array_key_exists($_POST['id'], $_SESSION['order'])) {
                echo 'Корзина обновлена';
            } else {
                echo 'Товар добавлен в корзину';
            }
            $_SESSION['order'][$_POST['id']] = $_POST['count'];
        }
    }

    public function removeAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            unset($_SESSION['order'][$_POST['id']]);
        }
        echo 'Товар удален из корзины';

    }

    public function clearAction(){
        $_SESSION['update'] = array();
        $_SESSION['order'] = array();
        $odrer_products = Products::getOrderProducts();

        View::renderTemplate('Order/index.twig', [
            'admin' => $_SESSION['admin'],
            'products' => $odrer_products
        ]);

    }
    public function totalAction(){
        $total = 0;
        $odrer_products = Products::getOrderProducts();
        if($odrer_products){
            foreach ($odrer_products as $item) {
                $total = $total + $item['total'];
            }
        }

        echo $total;
    }

    public function checkoutAction()
    {
        $order_done = false;
        $order_name = 0;
        $need_attention = array();
        $total = 0;
        $order_products = array();
        $order_call = '';

        if (!empty($_SESSION['order'])) {
        $order_products = Products::getOrderProducts();
        $product_codes = array();

        foreach ($order_products as $item) {
            $product_codes[] = $item['code'];

        }

        $order_stock = Products::checkOrder($product_codes);
        $positions = array();
            $total = 0;
        foreach ($order_products as $key => $item) {
            $product_prices = json_decode($order_products[$key]['prices'],true);
            $order_products[$key]['product_price'] = (!$_SESSION['type']) ? $product_prices['BTS'] : $product_prices[$_SESSION['type']];
            $details = $order_stock[$item['code']];
            if ($details['stock'] == 0) {
                $order_products[$key]['edit'] = 0;
                $order_products[$key]['total'] = 0;
                unset($_SESSION['order'][$key]);
                $need_attention['delete'] = 1;
            } elseif ($item['count'] > $details['stock']) {
                $order_products[$key]['edit'] = $details['stock'];
                $order_products[$key]['total'] = $details['stock'] * $order_products[$key]['product_price'];
                $need_attention['change'] = 1;
            }
            $order_products[$key]['pid'] = $details['pid'];
            $price = $order_products[$key]['product_price'];
            $total = $total + (int)$item['total'];

            $positions[] = [
                "quantity" => (int)$item['count'],
                "price" => $price * 100,
                "discount" => 0,
                "vat" => 0,
                "assortment" => [
                    "meta" => [
                        "href" => "https://online.moysklad.ru/api/remap/1.1/entity/product/" . $details['pid'],
                        "type" => "product",
                        "mediaType" => "application/json"
                    ]
                ],
                "reserve" => (int)$item['count'],
            ];
        }


        if (!isset($need_attention['delete']) && !isset($need_attention['change'])) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $credits = [
                    'organization' => [
                        'meta' => [
                            'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/organization/82dedc23-723d-11e9-9109-f8fc000b02be',
                            'type' => 'organization',
                            'mediaType' => 'application/json'
                        ],
                    ],
                    'agent' => [
                        'meta' => [
                            'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/counterparty/' . $_SESSION['mid'],
                            'type' => 'counterparty',
                            'mediaType' => 'application/json'
                        ],
                    ],
                    'store' => [
                        'meta' => [
                            'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/store/eee9eb1c-2c9c-11e8-9107-5048000f335f',
                            'type'=> 'store',
                            'mediaType' => 'application/json'
                        ]
                    ],
                ];

                $credits['positions'] = $positions;
                $credits['description'] = '';
                $order_call = (isset($_POST['order_call'])) ? $_POST['order_call'] : '';
                if ($order_call !== "nocall") {
                    $credits['description'] .= "Требуется звонок! ";
                }
                $credits['description'] .= "Доставка на " . $_POST['order_date'] . ". Время доставки: " . $_POST['order_time'] . ". Способ оплаты: " . $_POST['order_pay'] . ". ";
                if ($_POST['order_desc'] !== "") {
                    $credits['description'] .= "Комментарий: " . $_POST['order_desc'];
                }


                $order_done = true;

                $new_order = Moysklad::newOrder($credits);
                $order_info = json_decode($new_order,true);
                $order_name = $order_info['name'];
                $now = date("Y-m-d H:i:s");
                User::saveUserOrder($_SESSION['uid'], $order_name, json_encode($order_products,JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK), $now, $total, $credits['description']);

                $_SESSION['update'] = array();
                $_SESSION['order'] = array();
            }
        }



            View::renderTemplate('Order/checkout.twig', [
                'admin' => $_SESSION['admin'],
                'products' => $order_products,
                'total' => $total,
                'need_attantion' => $need_attention,
                'order_done' => $order_done,
                'order_name' => $order_name,
                'order_call' => $order_call
            ]);

    }else{
           $this->clearAction();
        }

    }


    public function orderListAction()
    {
        $uid = $_SESSION['uid'];
        $orders = User::getUserOrders($uid);
        View::renderTemplate('Order/list.twig', [
            'admin' => $_SESSION['admin'],
            'orders' => $orders,
        ]);
    }

    public function orderShowAction()
    {
        $id = $this->route_params['id'];
        $order = User::getUserOrder($id);
        if($order['uid'] == $_SESSION['uid']){
            View::renderTemplate('Order/show.twig', [
                'admin' => $_SESSION['admin'],
                'products' => json_decode($order['body']),
                'total' => $order['total'],
                'name' => $order['name'],
                'date' => $order['date'],
                'description' => $order['description'],
                'back_user' => '/orders',
            ]);
        } else {
            header('HTTP/1.1 200 OK');
            header('Location: /orders');
            exit;
        }


    }
}
