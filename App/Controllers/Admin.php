<?php

namespace App\Controllers;

use App\Config;
use App\Models\Actions;
use App\Models\Moysklad;
use \Core\View;
use App\Models\User;
use App\Models\Products;
use XLSXWriter;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Admin extends \Core\Controller
{
    protected function before()
    {
        self::startSession();

        if(!isset($_SESSION['user'])){
            header('HTTP/1.1 200 OK');
            header('Location: /login/exit');
            exit;
        }
        if(!isset($_SESSION['admin'])){
            header('HTTP/1.1 200 OK');
            header('Location: /');
            exit;
        }

    }


    public function indexAction()
    {
        View::renderTemplate('Admin/index.twig',['admin'=> $_SESSION['admin']]);
    }

    public function usersAction()
    {
        $cat = User::getAll();
        View::renderTemplate('Admin/users.twig',['admin'=> $_SESSION['admin'], 'cat' => $cat, 'back_user' => '/dsb']);
    }

    public function usereditAction()
    {
        $id = $this->route_params['id'];
        $user = User::getUserById($id);
        $orders = User::getUserOrderStat($id);
        $path = parse_url($_SERVER['HTTP_REFERER'])['path'];
        $back_user = ($path == '/dsb/orders' )?'/dsb/orders' : '/dsb/users';
        View::renderTemplate('Admin/user.twig',['admin'=> $_SESSION['admin'], 'user' => $user, 'orders' => $orders, 'back_user' => $back_user]);
    }
    public function usernewAction()
    {
        $user = array('id' => 'new', 'name' => '', 'mid' => '');
        $back_user = '/dsb/users';
        View::renderTemplate('Admin/user.twig',['admin'=> $_SESSION['admin'], 'user' => $user, 'back_user' => $back_user]);
    }

    public function saveAction()
    {
        $data = $_POST;
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $url = 'https://online.moysklad.ru/api/remap/1.2/entity/counterparty/'.$_POST['mid'];
        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));
        $user = file_get_contents($url, false, $context);
        $data['type'] = json_decode($user)->priceType->name;

        if($data['id'] !== 'new') {
            $message = User::updateUser($data);

        } else {
            $message = User::insertUser($data);
        }
        echo $message;
    }

    public function deleteAction()
    {
        $data = $_POST;
        if($data['id'] !== 'new') {
            $message = User::deleteUser($data['id']);

        }
        echo $message;
    }

    public function orderListAction(){
        $id = $this->route_params['id'];
        $user = User::getUserById($id);
        $orders = User::getUserOrders($id);
        $back_user = (parse_url($_SERVER['HTTP_REFERER'])['path'] == '/dsb/users' )?'/dsb/users' : '/dsb/user/'.$id;
        View::renderTemplate('Admin/orders.twig', [
            'admin' => $_SESSION['admin'],
            'orders' => $orders,
            'user' => $user,
            'back_user' => $back_user,
        ]);
    }

    public function orderShowAction()
    {
        $id = $this->route_params['id'];
        $num = $this->route_params['num'];
        $order = User::getUserOrder($num);
        $user = User::getUserById($id);

        $back_user = parse_url($_SERVER['HTTP_REFERER'])['path'];
            View::renderTemplate('Admin/order.twig', [
                'admin' => $_SESSION['admin'],
                'products' => json_decode($order['body']),
                'total' => $order['total'],
                'name' => $order['name'],
                'date' => $order['date'],
                'description' => $order['description'],
                'back_user' => $back_user,
                'user' => $user
            ]);

    }

    public function orderAllAction(){
        $start = '';
        $end = '';
        if ($_POST){
            $start = $_POST['order_start'];
            $end = $_POST['order_end'];
        }
        $orders = User::lastOrders($start, $end);
        $back_user = '/dsb';
        View::renderTemplate('Admin/ordersall.twig', [
            'admin' => $_SESSION['admin'],
            'orders' => $orders,
            'back_user' => $back_user,
            'start' => $start,
            'end' => $end,
        ]);
    }

    public function descTosSkladAction(){
        $products = Products::getAllProducts();
        $formated = array();
        foreach ($products as $product){
            $formated[$product['mid']] = array('description' => $product['name'].'|'.$product['desc']);
            $result = Moysklad::updateDescr($product['mid'], $formated[$product['mid']]);
            if($result){
                sleep(1);
            }
        }
        //print_r($formated);
        View::renderTemplate('Admin/update.twig', [
            'admin' => $_SESSION['admin'],
            'products' => $formated
        ]);
    }

    public function catalogAction()
    {
        $id = $this->route_params['id'];
        $message = "";
        if(isset($_GET['delete'])) {
            Products::deleteCat($_GET['delete']);
            header( "Location: /dsb/category/". $id );
        }

        if(isset($_GET['hide'])) {
            Products::hideCat($_GET['hide']);
            header( "Location: /dsb/category/". $id );
        }

        if(isset($_GET['show'])) {
            Products::showCat($_GET['show']);
            header( "Location: /dsb/category/". $id );
        }

        if($_SERVER['REQUEST_METHOD'] === 'POST') {
           // print_r($_POST);
            if($_POST['name']){
                Products::addCategory($id, $_POST['name']);
                $message .= 'Категория изменена</br>';
            }
            if($_POST['subcategory']){
                Products::addSubcategory($id, $_POST['subcategory']);
                $message .= 'Подкатегория добавлена</br>';
            }
            if($_POST['products']){
                $products = explode(',',$_POST['products']);
                Products::addCatProducts($id, $products);
                $message .= 'Продукты добавлены</br>';
            }
            $_POST = array();

        }
        if($id == 0){
            //$catalog = Products::getCatalog();
            $currentCat = array('id'=> 0, 'name' => 'Категории');
            $catChildrens = Products::getChildrens($id);

            View::renderTemplate('Admin/category.twig', [
                'admin' => $_SESSION['admin'],
                'cat' => $catChildrens,
                'currentCat' => $currentCat,
                'message' => $message,
                'back_user' => '/dsb',
            ]);
        } else {
            $catalog = Products::getCatalog();
            $currentCat = $catalog[$id][0];
            $catChildrens = Products::getChildrens($id);

            View::renderTemplate('Admin/category.twig', [
                'admin' => $_SESSION['admin'],
                'cat' => $catChildrens,
                'currentCat' => $currentCat,
                'message' => $message,
                'back_user' => '/dsb/category/'.$currentCat['parent']
            ]);
        }

    }

    public function actionsAction(){
        $id = $this->route_params['id'];
        $message = "";
        if(isset($_GET['delete'])) {
            Actions::deleteAction($_GET['delete']);
            header( "Location: /dsb/actions/". $id );
        }
        if($_SERVER['REQUEST_METHOD'] === 'POST') {
            Actions::addAction($id, $_POST['text'], $_POST['url']);
            $message .= ($id == 0)? 'Акция добавлена</br>' : 'Акция обновлена</br>';
            $_POST = array();
        }
        if($id == 0){
            $actions = Actions::getAll();
            $currentAction = array('id'=> 0, 'name' => 'Акции');
            View::renderTemplate('Admin/actions.twig', [
                'admin' => $_SESSION['admin'],
                'actions' => $actions,
                'currentAction' => $currentAction,
                'message' => $message,
                'back_user' => '/dsb',
            ]);
        }else{
            $actions = array();
            $currentAction = Actions::getAction($id);
            $currentAction['name'] = "Акция ".$id;
            View::renderTemplate('Admin/actions.twig', [
                'admin' => $_SESSION['admin'],
                'actions' => $actions,
                'currentAction' => $currentAction,
                'message' => $message,
                'back_user' => '/dsb/actions/0',
            ]);
        }
    }

    public function xlsAction(){
        $users = User::getAllStat();
        $writer = new XLSXWriter;
        $filename = "report.xlsx";
        $writer->setAuthor('BTS');
        $row_options = array('height'=>15,'wrap_text'=>true);
        $col_options = ['widths'=>[20,20,20,20,20,20], 'freeze_rows'=>1];
        $header = array(
            'Контрагент'=>'string',
            'Телефон'=>'string',
            'Кол-во заказов'=>'string',
            'Последний заказ'=>'string',
            'Последний вход'=>'string',
            'Создан'=>'string',
            'Почта' => 'string'
        );
        $writer->writeSheetHeader('Все', $header,$col_options );
        foreach ($users as $row){
            $writer->writeSheetRow('Все', array($row['name'],$row['phone'], $row['orders'],$row['lastorder'],($row['last']) ? date('d.m.Y', strtotime($row['last'])) : '', ($row['reg']) ? date('d.m.Y', strtotime($row['reg'])) : '', $row['mail']), $row_options);
        }
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $writer->writeToStdOut();
        exit(0);

    }

}
