<?php

namespace App\Controllers;

use \Core\View;
use App\Config;
use App\Models\Moysklad;
use App\Models\Products;
use \DateTime;

class Webhook extends \Core\Controller
{
    public function updateAction(){
        $updates = json_decode(file_get_contents('php://input'))->events;
        foreach ($updates as $update){
            $productUrl = $update->meta->href;
            $product = json_decode(Moysklad::getProductInfo($productUrl));

            if(Products::checkProduct($product->code)){
                //print_r($product);
                //цены
                foreach ($product->salePrices as $price ){
                    if($price->priceType->name == "BTS"){
                        $bts_price = $price->value / 100;
                    }
                    if($price->priceType->name == "Розница"){
                        $rozn_price = $price->value / 100;
                    }
                }

                $images = $product->images->rows;
                $uploadPath = '/var/www/bts.st/public/uploads/'.$product->code;
                $file = glob($uploadPath.'.{jpg,png,gif}',GLOB_BRACE);

                if(empty($images) && !empty($file)){ //стереть файл
                    unlink($file[0]);
                    Products::updateImage($product->code,'remove', '');
                }elseif (!empty($images) && !empty($file)) { //проверить и заменить
                    $image = $images[0];
                    $createTime = new DateTime();
                    $createTime->setTimestamp(filectime($file[0]));
                    $createTime = $createTime->format('Y-m-d H:i:s');
                    $updateTime = new DateTime($image->updated);
                    $updateTime = $updateTime->format('Y-m-d H:i:s');
                    if($updateTime > $createTime) {
                        unlink($file[0]);
                        $fileNameArr = explode(".", $image->filename);
                        $fileExt = end($fileNameArr);
                        $path = $uploadPath.'.'.$fileExt;
                        Moysklad::getProductImage($image->meta->downloadHref, $path);
                        Products::updateImage($product->code,'update', $fileExt);
                    }

                }elseif (!empty($images) && empty($file)){

                    $image = $images[0];
                    $fileNameArr = explode(".", $image->filename);
                    $fileExt = end($fileNameArr);
                    $path = $uploadPath.'.'.$fileExt;
                    Moysklad::getProductImage($image->meta->downloadHref, $path);
                    Products::updateImage($product->code,'update', $fileExt);
                }



                $mid = $product->id;

            }
            sleep(1);
        } //end foreach
    }
}
