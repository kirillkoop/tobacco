<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\Targetsms;
use \Core\View;
use App\Config;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


/**
 * Home controller
 *
 * PHP version 7.0
 */
class Login extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Login/index.twig');
        ///print_r($_SESSION);
       // phpinfo();
    }

    public function authcodeAction()
    {
        if(isset($_POST['user']) && isset($_POST['code'])){
            $user = User::getUser($_POST['user']);
            if ($user->code == $_POST['code']) {
                self::startSession();
                $_SESSION['uid'] = $user->id;
                $_SESSION['user'] = $user->name;
                $_SESSION['mid'] = $user->mid;
                $_SESSION['phone'] = $user->phone;
                $_SESSION['admin'] = $user->admin;
                $_SESSION['type'] = $user->type;
                $_SESSION['update'] = array();
                $_SESSION['order'] = array();
                User::saveLoginTime($user->id);

                echo "login";
            } else {
                echo "fail";
            }

        }

    }
    public function authAction()
    {
        if(isset($_POST['user'])){
            self::startSession();

            $user = User::getUser($_POST['user']);
            $_SESSION['uid'] = $user->id;
            $_SESSION['user'] = $user->name;
            $_SESSION['mid'] = $user->mid;
            $_SESSION['phone'] = $user->phone;
            $_SESSION['admin'] = $user->admin;
            $_SESSION['type'] = $user->type;
            $_SESSION['update'] = array();
            $_SESSION['order'] = ($_SESSION['order']) ? $_SESSION['order'] : array();
            User::saveLoginTime($user->id);
        }

    }

    public function existAction()
    {
        if(isset($_POST['user'])){
            if(!User::getUser($_POST['user'])){
                echo 'false';
            }
        }
    }

    public function checkAction()
    {
        if(isset($_POST['user'])){
            $user = User::getUser($_POST['user']);
            $message = (object) array(
                'type' => '',
                'value' => ''
                );
            if (!$user) {
                $message->type = false;
                $message->value = "Такого пользователя нет";
            } else {
                if($user['active']){
                    $message->type = 'code';
                    $message->value = $user['phone'];
                }else{
                    $message->type = 'setcode';
                    $message->value = $user['phone'];
                  /* try {
                        $result = Targetsms::generateCode($user['phone']);
                        $code = $result->success->attributes()['code']->__toString();
                        $message->type = 'sms';
                        $message->value = base64_encode($code);
                    } catch (Exception $e) {
                        $error = $e->getMessage();//ловим ошибку от сервера
                        $message->type = 'error';
                        $message->value = $error;
                    }*/
                }
            }
            echo json_encode($message);
        }
    }

    public function setcodeAction()
    {
        if(isset($_POST['phone']) && isset($_POST['mail']) && isset($_POST['code'])){
            $email = $_POST['mail'];
            $phone = $_POST['phone'];
            $code = hash("sha512", $_POST['code']);
            $hash = User::setpassUser(array('code' => $code, 'mail' => $email, 'phone' => $phone, 'active' => 1));
            $mail = new PHPMailer(true);
            try {
                //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                $mail->isSMTP();                                            // Send using SMTP
                $mail->Host       = 'ssl://smtp.mail.ru';                    // Set the SMTP server to send through
                $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                $mail->Username   = 'site@bts.st';                     // SMTP username
                $mail->Password   = 'Ybxtuj--15';                               // SMTP password
                $mail->SMTPSecure = 'SSL';
                $mail->Port = '465';

                $mail->setFrom('site@bts.st', 'Сайт BTS');
                $mail->addAddress($email);

                $mail->CharSet = 'UTF-8';
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Подтверждение адреса почты';
                $mail->Body    = '<p>Ваш пароль coхранен. Для возможности восстановления забытого пароля, подтвердите адрес элетронной почты.</p>
                <p>перейдите по ссылке: <a href="https://bts.st/profile/activate?code='.$hash.'&phone='.$phone.'">Подвердить emil</a></p>';
                $mail->send();
                echo $phone;
            }
            catch (Exception $e) {
                echo "error";
            }
        }
    }
    public function passAction()
    {
        if(isset($_POST['phone']) && isset($_POST['code'])){
            $user = User::getUser($_POST['phone']);
            if(hash("sha512", $_POST['code']) == $user['code'] ){
                echo $_POST['phone'];
            }else{
                echo 'error';
            }
        }
    }

    public function resetAction()
    {
        if(isset($_POST['user'])){
            $user = User::getUser($_POST['user']);
            if($user['active'] == 1){
                echo '/profile/reset?status=notactive';
            }elseif($user['active'] == 2){
                function generateRandomString($length = 6) {
                    $characters = '0123456789';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return $randomString;
                }
                $code = generateRandomString();
                User::resetUser( $user['id'],  $code);
                $mail = new PHPMailer(true);
                try {
                    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = 'ssl://smtp.mail.ru';                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $mail->Username   = 'site@bts.st';                     // SMTP username
                    $mail->Password   = 'Ybxtuj--15';                               // SMTP password
                    $mail->SMTPSecure = 'SSL';
                    $mail->Port = '465';

                    $mail->setFrom('site@bts.st', 'Сайт BTS');
                    $mail->addAddress($user['mail']);

                    $mail->CharSet = 'UTF-8';
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Временный пароль';
                    $mail->Body    = '<p>Ваш временный пароль: '.$code.'</p>
                    <p><a href="https://bts.st/">Перейти на сайт</a></p>';
                    $mail->send();
                }
                catch (Exception $e) {
                    echo "error";
                    exit;
                }

                echo '/profile/reset?status=active';
            }
        }

    }
    public function resetresultAction(){
        View::renderTemplate('Profile/reset.twig', [
            'status' => $_GET['status'],
        ]);
    }

    public function activateAction(){

        $user = User::getUser('+'.trim($_GET['phone']));
        $code = $_GET['code'];
        if($user['code'] == $code){
            User::activateUser($user['phone']);
            View::renderTemplate('Profile/activate.twig');
        }
    }

    public function profileAction(){
        self::startSession();
       //print_r($_SESSION);
        if(!isset($_SESSION['user'])){
            header('HTTP/1.1 200 OK');
            header('Location: /login/exit');
            exit;
        }
        $user = User::getUser($_SESSION['phone']);
        $message = null;
        $email = $user['mail'];
        if($user['active'] == 1){
            $message="Необходимо подтвердить email. Проверьте почту.";
        }

        if($_POST){
            if ($_POST['mail'] == $user['mail']){
                if($_POST['code']){
                    $code = hash("sha512", $_POST['code']);
                    User::setpassUser(array('code' => $code, 'mail' => $user['mail'], 'phone' => $user['phone'], 'active' => $user['active']));
                    $message = 'Пароль сохранен';
                }
            }else{
                $email = $_POST['mail'];
                if($_POST['code']){
                    $code = hash("sha512", $_POST['code']);
                    User::setpassUser(array('code' => $_POST['code'], 'mail' => $email, 'phone' => $user['phone'], 'active' => 1));
                    $message = 'Пароль сохранен; Необходимо подтвердить email. Проверьте почту';
                }else{
                    $code = $user['code'];
                    User::setpassUser(array('code' => $user['code'], 'mail' => $email, 'phone' => $user['phone'], 'active' => 1));
                    $message = 'Необходимо подтвердить email. Проверьте почту';
                }
                $mail = new PHPMailer(true);
                try {
                    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = 'ssl://smtp.mail.ru';                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $mail->Username   = 'site@bts.st';                     // SMTP username
                    $mail->Password   = 'Ybxtuj--15';                               // SMTP password
                    $mail->SMTPSecure = 'SSL';
                    $mail->Port = '465';

                    $mail->setFrom('site@bts.st', 'Сайт BTS');
                    $mail->addAddress($_POST['mail']);

                    $mail->CharSet = 'UTF-8';
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Подтверждение адреса почты';
                    $mail->Body    = '<p>Подтвердите новый email</p>
                    <p><a href="https://bts.st/profile/activate?code='.$code.'&phone='.$user['phone'].'">Подвердить emil</a></p>';
                    $mail->send();
                }
                catch (Exception $e) {
                    echo "error";
                }
            }

        }
        View::renderTemplate('Profile/index.twig', [
            'admin' => $_SESSION['admin'],
            'phone' => $user['phone'],
            'id' => $user['id'],
            'mail' => $email,
            'message' => $message
        ]);
    }

    public function skladAction()
    {
        $rest = '?limit=5000&filter=code=2114;';
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $url = 'https://online.moysklad.ru/api/remap/1.1/entity/assortment'.$rest;

        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));
        file_get_contents($url, false, $context);
        $http_code = substr($http_response_header[0], 9, 3);
        //$http_code = 503;
        if ($http_code == 200){
            echo 'true';
        } else {
            echo 'false';
        }

    }

    public function regAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $mail = new PHPMailer(true);
            try {
                //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                $mail->isSMTP();                                            // Send using SMTP
                $mail->Host       = 'ssl://smtp.mail.ru';                    // Set the SMTP server to send through
                $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                $mail->Username   = 'site@bts.st';                     // SMTP username
                $mail->Password   = 'Ybxtuj--15';                               // SMTP password
                $mail->SMTPSecure = 'SSL';
                $mail->Port = '465';

                $mail->setFrom('site@bts.st', 'Сайт BTS');
                $mail->addAddress('Sb.bts@mail.ru');
                $mail->addAddress('karasev_a@list.ru');
                $mail->addAddress('Sviridov@bts.st');
                //$mail->addAddress('kirill@bearscience.net');

                $mail->CharSet = 'UTF-8';
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Заявка на регистрацию';
                $mail->Body    = 'Контактное лицо: '.$_POST['contact'].'<br/>Телефон: '.$_POST['phone'].'<br/>Заведение: '.$_POST['company'].'<br/>Описание: '.$_POST['company_desc'];
                $mail->send();
                echo "Сообщение уже у нас, мы скоро перезвоним вам!";
            }
            catch (Exception $e) {
                echo "Ошибка отправки попробуйте еще раз";
            }
        }

    }

    public function exitAction()
    {
        self::clearSession();
        View::renderTemplate('Login/index.twig', ['body_class' => 'exit' ]);

    }
    public function errorAction()
    {
        View::renderTemplate('Login/index.twig', ['body_class' => 'error' ]);

    }
    public function codeAction(){
        $id = $this->route_params['id'];
        View::renderTemplate('Login/code.twig', ['phone' => $id ]);
    }
}
