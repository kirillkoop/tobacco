<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Actions;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends \Core\Controller
{
    protected function before()
    {
        self::startSession();
        if(!isset($_SESSION['user'])){
            header('HTTP/1.1 200 OK');
            header('Location: /login/exit');
            exit;
        }
    }

    public function indexAction()
    {
        $customer = (isset($_SESSION['user'])) ? $_SESSION['user'] : '';
        $admin = (isset($_SESSION['admin'])) ? $_SESSION['admin'] : 0;
        $actions = Actions::getAll();
        $cat = [['name' => '!Распродажа!', 'id'=>'144'], ['name' => 'Табак', 'id'=>'1'], ['name' => 'Уголь', 'id'=>'2'] , ['name' => 'Аксессуары', 'id'=>'3'] , ['name' => 'Кальяны', 'id'=>'113']];

        View::renderTemplate('Home/index.twig',['admin'=> $admin, 'cat' => $cat, 'customer' => $customer, 'actions' => $actions]);
    }


}
