<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Products;

class Catalog extends \Core\Controller
{
    protected function before()
    {
        self::startSession();
        if(!isset($_SESSION['user'])){
            header('HTTP/1.1 200 OK');
            header('Location: /login/exit');
            exit;
        }
        $_SESSION['update'] = (isset($_SESSION['update'])) ? $_SESSION['update'] : array();
        $_SESSION['order'] = (isset($_SESSION['order'])) ? $_SESSION['order'] : array();

    }

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $id = $this->route_params['id'];
        $updated = in_array($id, $_SESSION['update']);
        if (!$updated) {
            Products::updateProducts($id);
        }
        $catalog = Products::getCatalog();
        $currentCat = $catalog[$id][0];
        $path = Products::pathToCat($catalog, $currentCat['parent']);
        $catChildrens = Products::getChildrens($id);
        $updated = in_array($id, $_SESSION['update']);
        $catProducts = ($updated) ? Products::getProducts($id) : array();

        foreach ($catProducts as $key => $value){
            $product_prices = json_decode($value['prices'],true);
            $catProducts[$key]['product_price'] = (!$_SESSION['type']) ? $product_prices['BTS'] : $product_prices[$_SESSION['type']];
        }

        View::renderTemplate('Catalog/index.twig', [
            'admin' => $_SESSION['admin'],
            'type' => $_SESSION['type'],
            'cat' => $catChildrens,
            'currentCat' => $currentCat,
            'products' => $catProducts,
            'path' => $path,
            'back' => $currentCat['parent'],
            'order' => $_SESSION['order']
        ]);
    }

    public function productAction()
        {
            $id = $this->route_params['id'];
            $cat = $this->route_params['cat'];
            $back = $_SERVER['HTTP_REFERER'];
            $catalog = Products::getCatalog();
            $currentCat = $catalog[$cat][0];
            $product = Products::getProduct($id);
            $path = Products::pathToCat($catalog, $currentCat['id']);
            $in_order = (array_key_exists($id, $_SESSION['order'])) ? $_SESSION['order'][$id] : 0;
            //print_r($product['prices']);
            $product_prices = json_decode($product['prices'],true);
            $product['product_price'] = (!$_SESSION['type']) ? $product_prices['BTS'] : $product_prices[$_SESSION['type']];
            View::renderTemplate('Catalog/product.twig', [
                'admin' => $_SESSION['admin'],
                'type' => $_SESSION['type'],
                'currentCat' => $currentCat,
                'path' => $path,
                'product' => $product,
                'in_order' => $in_order,
                'back_product' => $back
            ]);
        }


}
