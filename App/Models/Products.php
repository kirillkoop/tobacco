<?php

namespace App\Models;
use App\Config;
use PDO;


class Products extends \Core\Model
{
    public static function getCat($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_category WHERE id = ?');
        $stmt->execute([$id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function getCatalog()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM tbc_category');
        $catalog = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $tree = array();
        foreach ($catalog as $value) {
            $tree[$value['id']][] = $value;
        }
        return $tree;
    }

    public static function pathToCat($catlog, $id, $type = 'topbar'){
        if (is_array($catlog) && isset($catlog[$id])) {
            $path = '';
            foreach ($catlog[$id] as $cat) {
                $path .= self::pathToCat($catlog, $cat['parent'], $type);

                if($type == 'topbar')
                {
                    $path .='<a href="/catalog/'.$cat['id'].'">'.$cat['name'].'</a>';
                } else {
                    $path .= $cat['name'].' ';
                }
            }
        } else {
            return false;
        }
        return $path;
    }

    public static function getChildrens($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_category WHERE parent = ? ORDER BY name');
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getProducts($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_products WHERE cat = ? ORDER BY name');
        $stmt->execute([$id]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getProduct($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_products WHERE id = ?');
        $stmt->execute([$id]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0];
    }

    public static function checkProduct($code)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_products WHERE code = ?');
        $stmt->execute([$code]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return (!empty($result) && $result[0]['code'] == $code) ? true : false;
    }

    public static function setPrice($prices)
    {
        return $prices[$_SESSION[type]];
    }

    public static function updateImage($code, $action, $ext)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_products SET img = ? WHERE code = ?');
        if($action == 'remove'){
            $stmt->execute(array('', $code));
        }else if($action == 'update'){
            $path = '/uploads/'.$code.'.'.$ext;
            $stmt->execute(array($path, $code));
        }

    }




    private static function getStock($list){
        $rest = '?limit=5000&filter=';
        foreach ($list as $item){
            $rest .= 'code='.$item.';';
        }
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $url = 'https://online.moysklad.ru/api/remap/1.1/entity/assortment'.$rest;

        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));
        $data = file_get_contents($url, false, $context);
        $http_code = substr($http_response_header[0], 9, 3);
        //$http_code = 503;
        if ($http_code == 200){
            return json_decode($data);
        } else {
            header('HTTP/1.1 200 OK');
            header('Location: /login/error');
            exit;
        }

    }




    public static function updateProducts($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT code FROM tbc_products WHERE cat = ?');
        $stmt->execute([$id]);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN);

        if ($result) {

            $formated = self::getStock($result);
            $stmt = $db->prepare('UPDATE tbc_products SET name = ?, `desc` = ?, stock = ?, price = ?, price2 = ?, prices = ?, mid = ? WHERE code = ?');
            foreach($formated->rows as $good)
            {
                $bts_prices = $good->salePrices;
                $desc = explode("|", $good->description);
                $bts_price = 0;
                $rozn_price = 0;
                $prices = array();
                foreach ($bts_prices as $price ){
                    $prices[$price->priceType] = $price->value / 100;

                    if($price->priceType == "BTS"){
                        $bts_price = $price->value / 100;
                    }
                    if($price->priceType == "Розница"){
                        $rozn_price = $price->value / 100;
                    }
                }
                $pricesJson = json_encode($prices, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
                $availible = $good->stock - $good->reserve;
                $stmt->execute(array($desc[0], $desc[1], $availible, $bts_price, $rozn_price, $pricesJson, $good->id, $good->code));

            }
            $_SESSION['update'][] = $id;
        }
    }

    public static function checkOrder($list)
    {
        $formated = self::getStock($list);
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_products SET stock = ?, price = ?, price2 = ?, prices = ? WHERE code = ?');
        $order_stock = array();
        foreach($formated->rows as $good)
        {
            $bts_prices = $good->salePrices;
            $bts_price = 0;
            $rozn_price = 0;
            $prices = array();
            foreach ($bts_prices as $price ){
                $prices[$price->priceType] = $price->value / 100;
                if($price->priceType == "BTS"){
                    $bts_price = $price->value / 100;
                }
                if($price->priceType == "Розница"){
                    $rozn_price = $price->value / 100;
                }
            }
            $availible = $good->stock - $good->reserve;
            $pricesJson = json_encode($prices, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
            $stmt->execute(array($availible, $bts_price, $rozn_price, $pricesJson, $good->code));
            //$order_stock[$good->code] = array('pid' => $good->id, 'stock' => 0,);
            $order_stock[$good->code] = array('pid' => $good->id, 'stock' => $availible,  );

        }
        return $order_stock;
    }

    public static function getOrderProducts()
    {
        $ids = array_keys($_SESSION['order']);
        if ($ids){
            $catalog = self::getCatalog();
            $db = static::getDB();
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $sql = "SELECT * FROM tbc_products WHERE id IN ($in)";
            $stmt = $db->prepare($sql);
            $stmt->execute($ids);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $addParams = function($row) use ($catalog) {
                $row['count'] = $_SESSION['order'][$row['id']];
                $row['path'] = self::pathToCat($catalog, $row['cat'], 'order');
                $product_prices = json_decode($row['prices'],true);
                $product_price = (!$_SESSION['type']) ? $product_prices['BTS'] : $product_prices[$_SESSION['type']];
                $row['total'] = $row['count'] * $product_price;
                return $row;
            };
            $res = array_map($addParams, $result);

            $path = array_column($res, 'path');
            $names = array_column($res, 'name');
            array_multisort($path, SORT_ASC, $names, SORT_ASC,  $res);
            return $res;
        }
        return false;
    }

    public static function getAllProducts()
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_products LIMIT 100 OFFSET 1000');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function upAllProducts()
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_products');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $hungred = array_chunk($result, 100, true);
        //print_r($hungred[0]);
        $rest = '?limit=100&filter=';

        foreach ($hungred[0] as $item) {
            $rest .= 'code=' . $item['code'] . ';';
        }
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $url = 'https://online.moysklad.ru/api/remap/1.1/entity/assortment' . $rest;

        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));

        $data = json_decode(file_get_contents($url, false, $context));
        //return json_decode($data);
        //print_r($data->rows[0]->id);

        $stmt = $db->prepare('UPDATE tbc_products SET mid = ? WHERE code = ?');
        foreach ($data->rows as $item) {
            $stmt->execute(array($item->id, $item->code));
        }
    }


    public static function addCategory($id, $name){
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_category SET name = ? WHERE id = ?');
        $stmt->execute(array($name, $id));
    }

    public static function hideCat($id){
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_category SET `show` = 1 WHERE id = ?');
        $stmt->execute(array($id));
    }

    public static function showCat($id){
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_category SET `show` = 0 WHERE id = ?');
        $stmt->execute(array($id));
    }

    public static function addSubcategory($id, $name){
        $db = static::getDB();
        $stmt = $db->prepare('INSERT tbc_category SET name = ?, parent = ?');
        $stmt->execute(array($name, $id));
    }

    public static function addCatProducts($id, $products){
        $db = static::getDB();
        $stmt = $db->prepare('INSERT tbc_products SET cat = ?, code = ?');
        foreach($products as $product){
            $stmt->execute(array($id, $product));
        }

    }

    public static function deleteCat($id){
        $db = static::getDB();
        $stmt = $db->prepare('DELETE FROM tbc_category WHERE id = ?');
        $stmt->execute(array($id));
        $stmt2 = $db->prepare('DELETE FROM tbc_products WHERE cat = ?');
        $stmt2->execute(array($id));

    }
}
