<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Actions extends \Core\Model
{
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM tbc_actions WHERE active = 1');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getAction($id){
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_actions WHERE id = ?');
        $stmt->execute([$id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function addAction($id, $text, $url){
        $db = static::getDB();

        if($id == 0){
            $stmt = $db->prepare('INSERT tbc_actions SET text = ?, url = ?, active = 1');
            $stmt->execute(array($text, $url));
        }else{
            $stmt = $db->prepare('UPDATE tbc_actions SET `text` = ?, `url` = ? WHERE id = ?');
            $stmt->execute(array($text, $url, $id));
        }

    }
    public static function deleteAction($id){
        $db = static::getDB();
        $stmt = $db->prepare('DELETE FROM tbc_actions WHERE id = ?');
        $stmt->execute(array($id));
    }
}
