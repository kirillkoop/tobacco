<?php
namespace App\Models;
use Exception;
use App\Config;
class Targetsms extends \Core\Model {

public static function generateCode($phone)
{
$xml = '<?xml version="1.0" encoding="utf-8"?>
<request>
    <security>
        <login>'.Config::TARGETSMS_LOGIN.'</login>
        <password>'.Config::TARGETSMS_PASS.'</password>
    </security>
    <phone>'.$phone.'</phone>
    <sender>'.Config::TARGETSMS_SENDER.'</sender>
    <random_string_len>6</random_string_len>
</request>';
return self::send($xml);
}

public static function send($data)
{
$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CRLF, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_URL, Config::TARGETSMS_URL);
$result = curl_exec($ch);
$info = curl_getinfo($ch);
$error = curl_error($ch);
$info = curl_getinfo($ch);
curl_close($ch);

if (!isset($info['http_code']) || $info['http_code'] >= 400) {
throw new Exception('Ошибка запроса к серверу авторизации. Код: '.
$info['http_code']. '. Ошибка: '.$error);
}

$xml = @simplexml_load_string($result);
if (!$xml) {
throw new Exception('Неверный формат ответ от сервера.');
}

if (isset($xml->error)) {
throw new Exception($xml->error);
}

return $xml;

}

}
