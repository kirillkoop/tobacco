<?php

namespace App\Models;

use App\Config;
use PDO;


class Moysklad extends \Core\Model
{

    public static function newOrder($credits){
        $orderPostData = json_encode($credits);
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $url = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder';


// Prepare new cURL resource
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $orderPostData);

// Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($orderPostData))
        );

// Submit the POST request
        $result = curl_exec($curl);
       // print_r($result);
// Close cURL session handle
        curl_close($curl);
        return $result;
    }

    public static function updateDescr($id,$desc){
        $putData = json_encode($desc,JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $url = 'https://online.moysklad.ru/api/remap/1.1/entity/product/'.$id;


// Prepare new cURL resource
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $putData);

// Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($putData))
        );

// Submit the POST request
        $result = curl_exec($curl);
        // print_r($result);
// Close cURL session handle
        curl_close($curl);
        return $result;
    }

    public static function getProductInfo($url){
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $imagesUrl = $url.'?expand=images';

        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));
        $data = file_get_contents($imagesUrl, false, $context);
        return $data;
    }

    public static function getProductImage($url,$path){
        $username = Config::MOYSKLAD_USERNAME;
        $password = Config::MOYSKLAD_PASSWORD;
        $curl = curl_init($url);
        $fp = fopen($path, 'wb');
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36');
        curl_setopt($curl, CURLOPT_FILE, $fp);
        curl_exec($curl);
        curl_close($curl);
        fclose($fp);

    }

}
