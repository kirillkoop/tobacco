<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class User extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM tbc_partners ORDER BY name');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getUser($phone)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_partners WHERE phone = ?');
        $stmt->execute([$phone]);
        return  $stmt->fetch(PDO::FETCH_LAZY);

    }

    public static function getUserById($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_partners WHERE id = ?');
        $stmt->execute([$id]);
        return  $stmt->fetch(PDO::FETCH_LAZY);

    }

    public static function updateUser($data)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_partners SET name = ?, phone = ?, mid = ?, active = ?, type = ? WHERE id = ?');
        $stmt->execute(array($data['name'], $data['phone'], $data['mid'], $data['code'], $data['type'], $data['id']));
        return "Контрагент обновлен";
    }

    public static function setpassUser($data)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_partners SET code = ?, mail = ?, active = ? WHERE phone = ?');
        $stmt->execute(array($data['code'], $data['mail'], $data['active'], $data['phone']));
        return $data['code'];
    }

    public static function activateUser($phone)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_partners SET active = ? WHERE phone = ?');
        $stmt->execute(array( 2, $phone));
    }

    public static function resetUser($id, $code)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_partners SET code = ? WHERE id = ?');
        $stmt->execute(array( hash("sha512", $code), $id));
    }


    public static function insertUser($data)
    {
        $db = static::getDB();
        $stmt = $db->prepare('INSERT tbc_partners SET name = ?, phone = ?, mid = ?, code = ?, type = ?, reg = ?');
        $stmt->execute(array($data['name'], $data['phone'], $data['mid'], $data['code'], $data['type'], date("Y-m-d H:i:s")));
        return "Контрагент добавлен";
    }

    public static function deleteUser($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('DELETE FROM tbc_partners WHERE id = ?');
        $stmt->execute(array($id));
        return "Контрагент удален";
    }

    public static function lastOrders($start, $end)
    {
        $db = static::getDB();
        if($start && $end){
            $sd = date('Y-m-d H:i:s', strtotime($start));
            $ed = date('Y-m-d H:i:s', strtotime($end. "+1 days"));
            $stmt = $db->prepare('SELECT o.id, o.uid, o.name, o.date, o.total, p.name `username` FROM `tbc_orders` o INNER JOIN `tbc_partners` p ON o.uid = p.id 
            WHERE o.date BETWEEN :start AND :end ORDER BY id DESC');
            $stmt->bindParam(':start', $sd, PDO::PARAM_STR);
            $stmt->bindParam(':end', $ed, PDO::PARAM_STR);
        }else{
            $stmt = $db->prepare('SELECT o.id, o.uid, o.name, o.date, o.total, p.name `username` FROM `tbc_orders` o INNER JOIN `tbc_partners` p ON o.uid = p.id ORDER BY id DESC LIMIT 20');
        }

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function allOrders()
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT id, uid, name, date, total FROM `tbc_orders`');
        $stmt->execute();
        $orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt2 = $db->prepare('UPDATE tbc_orders SET date = ? WHERE id = ?');
        foreach ($orders as $key => $value){
            $date = date('Y-m-d H:i:s', strtotime($value['date']));
            $orders[$key]['date'] = $date;
            $stmt2->execute(array($date, $value['id']));
        }
        return $orders;
    }

    public static function getUserOrders($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT id, name, date, total FROM tbc_orders WHERE uid = ? ORDER BY id DESC');
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getUserOrderStat($id){
        $orders = self::getUserOrders($id);
        if ($orders) {
            return array('count' => count($orders), 'last' => $orders[0]['date']);
        }
        return array('count' => '', 'last' => '');
    }


    public static function getUserOrder($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM tbc_orders WHERE id = ?');
        $stmt->execute([$id]);
        return $stmt->fetch(PDO::FETCH_LAZY);
    }

    public static function saveUserOrder($uid, $name, $body, $date, $total, $desc )
    {
        $db = static::getDB();
        $stmt = $db->prepare('INSERT tbc_orders SET uid = ?, name = ?, body = ?, date = ?, total = ?, description = ?');
        $stmt->execute(array($uid, $name, $body, $date, $total, $desc));
    }

    public static function saveLoginTime($id){
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE tbc_partners SET last = ? WHERE id = ?');
        $stmt->execute(array(date("Y-m-d H:i:s"), $id));
    }

    public static function getAllStat()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT p.id, p.name, p.phone, p.last, p.reg, p.mail  FROM tbc_partners p ORDER BY name');
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($users as $key => $value){
            $orders = self::getUserOrderStat($value['id']);
            $users[$key]['orders'] = $orders['count'];
            $users[$key]['lastorder'] = $orders['last'];
        }
        return $users;
    }
    public static function updateLogins(){
        $db = static::getDB();
        $stmt = $db->query('SELECT *  FROM tbc_logins');
        $stmt2 = $db->prepare('UPDATE tbc_partners SET last = ?, reg = ? WHERE phone = ?');
        $logins = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($logins as $key => $value){
            $phone = '+'.$value['phone'];
            $create = date("Y-m-d H:i:s", substr($value['create'], 0, 10));
            $login = date("Y-m-d H:i:s", substr($value['login'], 0, 10));
            $stmt2->execute(array($login, $create, $phone));
        }
    }



}
