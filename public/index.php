<?php
/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');
date_default_timezone_set('Europe/Moscow');

/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('request', ['controller' => 'Login', 'action' => 'reg']);
$router->add('login', ['controller' => 'Login', 'action' => 'index']);
$router->add('login/auth', ['controller' => 'Login', 'action' => 'auth']);
$router->add('login/exist', ['controller' => 'Login', 'action' => 'exist']);
$router->add('login/check', ['controller' => 'Login', 'action' => 'check']);
$router->add('login/set', ['controller' => 'Login', 'action' => 'setcode']);
$router->add('login/pass', ['controller' => 'Login', 'action' => 'pass']);
$router->add('login/reset', ['controller' => 'Login', 'action' => 'reset']);
$router->add('profile/activate', ['controller' => 'Login', 'action' => 'activate']);
$router->add('profile/reset', ['controller' => 'Login', 'action' => 'resetresult']);
$router->add('profile', ['controller' => 'Login', 'action' => 'profile']);
$router->add('catalog', ['controller' => 'Catalog', 'action' => 'index']);
$router->add('catalog/{id:\d+}', ['controller' => 'Catalog', 'action' => 'index']);
$router->add('catalog/{cat:\d+}/{id:\d+}', ['controller' => 'Catalog', 'action' => 'product']);
$router->add('order', ['controller' => 'Order', 'action' => 'index']);
$router->add('order/add', ['controller' => 'Order', 'action' => 'add']);
$router->add('order/remove', ['controller' => 'Order', 'action' => 'remove']);
$router->add('order/checkout', ['controller' => 'Order', 'action' => 'checkout']);
$router->add('order/clear', ['controller' => 'Order', 'action' => 'clear']);
$router->add('order/total', ['controller' => 'Order', 'action' => 'total']);
$router->add('orders', ['controller' => 'Order', 'action' => 'orderList']);
$router->add('orders/{id:\d+}', ['controller' => 'Order', 'action' => 'orderShow']);
$router->add('dsb', ['controller' => 'Admin', 'action' => 'index']);
$router->add('dsb/users', ['controller' => 'Admin', 'action' => 'users']);
$router->add('dsb/user/{id:\d+}', ['controller' => 'Admin', 'action' => 'useredit']);
$router->add('dsb/orders', ['controller' => 'Admin', 'action' => 'orderAll']);
$router->add('dsb/orders/{id:\d+}', ['controller' => 'Admin', 'action' => 'orderList']);
$router->add('dsb/orders/{id:\d+}/{num:\d+}', ['controller' => 'Admin', 'action' => 'orderShow']);
$router->add('dsb/user/new', ['controller' => 'Admin', 'action' => 'usernew']);
$router->add('dsb/user/update', ['controller' => 'Admin', 'action' => 'save']);
$router->add('dsb/user/delete', ['controller' => 'Admin', 'action' => 'delete']);
$router->add('dsb/user/xls', ['controller' => 'Admin', 'action' => 'xls']);
$router->add('dsb/update', ['controller' => 'Admin', 'action' => 'descTosSklad']);
$router->add('dsb/category/{id:\d+}', ['controller' => 'Admin', 'action' => 'catalog']);
$router->add('webhook/update', ['controller' => 'Webhook', 'action' => 'update']);
$router->add('dsb/actions/{id:\d+}', ['controller' => 'Admin', 'action' => 'actions']);

$router->add('{controller}/{action}');

$router->dispatch($_SERVER['QUERY_STRING']);
