var updated = [//тут указываем скрипты которые не должны жестко кешироваться
    "/sw.js"//указал сам скрипт service worker, чтобы не кешировался жестко в будущем. Проверил. Оффлайн режим работает без него в этом кеше.
];
const version = "0.0.7";//тут может быть номер вашей версии
const CACHE = "bts-cache" + version;//вместо soltyk - используйте свое уникальное название или доменное имя
self.addEventListener('install', function(event) {
    var indexPage = new Request('/login');
    event.waitUntil(fetch(indexPage).then(function(response) {
        var response2 = response.clone();
        return caches.open(CACHE).then(function(cache) {
            console.log('Cached index page during Install ' + response2.url);
            return cache.put(indexPage, response2)
        })
    }))
});
self.addEventListener('fetch', function(event) {
    const request = event.request;
    var updateCache = function(request) {
        return caches.open(CACHE).then(function(cache) {
            return fetch(request).then(
                function(response2 = response.clone()) {
                    if (updated.indexOf(response2.url) != -1){
                        console.log('Исключен из кеша ' + response2.url)
                    }else{
                        console.log('[PWA Builder] add page to offline ' + response2.url)
                        return cache.put(request.clone(), response2)
                    }
                })
        })
    };

    event.waitUntil(updateCache(request));
    event.respondWith(fetch(request).catch(function(error) {
        console.log('Network request Failed. Serving content from cache: ' + error);
        return caches.open(CACHE).then(function(cache) {
            return cache.match(request).then(function(matching) {
                var report = !matching || matching.status == 404 ? Promise.reject('no-match') : matching;
                return report
            })
        })
    }))
})
